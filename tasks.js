/**
 * 1. Напиши функцию createCounter, которая будет считать количество вызовов.
 * Нужно использовать замыкание.
 */

function createCounter() {
    let number = 0;
    return function() {
       return ++number;
    }
}

/**
 * 2. Не меняя уже написаный код (можно только дописывать новый),
 * сделай так, чтобы в calculateHoursOnMoon
 * переменная HOURS_IN_DAY была равна 29,5, а в функции calculateHoursOnEarth
 * эта переменная была 24.
 */

let HOURS_IN_DAY = 24;

function calculateHoursOnMoon (days) {
    let HOURS_IN_DAY = 29.5;
    return days * HOURS_IN_DAY;
}

function calculateHoursOnEarth (days) {
    return days * HOURS_IN_DAY;
}

/**
 * 3. Допиши функцию crashMeteorite, после которой
 * продолжительность дня на земле (из предыдущей задачи)
 * изменится на 22 часа
 */

function crashMeteorite () {
    HOURS_IN_DAY = 22;     
}

/**
 * 4. Функция createMultiplier должна возвращать функцию, которая
 * при первом вызове возвращает произведение аргумента
 * функции createMultiplier и переменной a, при втором — аргумента и b,
 * при третьем — аргумента и c, с четвертого вызова снова a, потом b и так далее.
 *
 * Например:
 * const func = createMultiplier(2);
 * func(); // 16 (2*a)
 * func(); // 20 (2*b)
 * func(); // 512 (2*c)
 * func(); // 16 (2*a)
 *
 */
function createMultiplier (num) {
    const a = 8;
    const b = 10;
    const c = 256;
    let count = 0;

    return function Multiplying() {
        if (count == 0) {
            count++;
            return num*a;
        }
        if (count == 1) {
            count++;
            return num*b;
        }
        if (count == 2) {
            count = 0;
            return num*c;
        }
    }
}

/**
 * 5. Напиши функцию createStorage, которая будет уметь хранить
 * какие-то данные и манипулировать ими.
 * Функция должна возвращать объект с методами:
 * - add — метод, который принимает на вход любое количество аргументов и добавляет их в хранилище;
 * - get — метод, возвращающий хранилище;
 * - clear — метод, очищающий хранилище;
 * - remove — метод, который принимает на вход элемент, который нужно удалить из хранилища и удаляет его;
 *
 * Примеры использования смотри в тестах.
 */

function createStorage () {
    //переменная mainStorage для хранения аргументов
    let mainStorage = [];
    return {
        add: function (...args) {
            for (let i = 0; i < args.length; i++) {
                mainStorage.push(args[i]);
            }
        },
        get: function () {
            return mainStorage;
        },
        clear: function () {
            mainStorage = [];
        },
        remove: function (forRemove) {
            let index = mainStorage.indexOf(forRemove);
            if (mainStorage.includes(forRemove)) {                
                if (forRemove && forRemove >= 0) {
                    mainStorage.splice(index, 1)
                }
            }
            if (mainStorage.includes(forRemove) && mainStorage.length == 1) {
                mainStorage = [];
            }
            //очень странная конструкция, не понимаю почему без нее не удаляется последний элемент массива
            if (mainStorage.includes(forRemove) && index == mainStorage.length - 1) {
                mainStorage.splice(mainStorage.length - 1, 1)
            }    
        }
    }
}

/**
 * 6*. Реализовать через let функцию поочередного
 * добавления в массив чисел от 0 до 10 с интервалом в 500ms.
 *
 * Для выполнения задачи должен быть цикл от 0 до 10,
 * внутри которого должен быть setTimeout(func, 50) с функцией внутри,
 * которая должна наполнить массив result числами от 0 до 10
*/

function letTimeout () {
    const result = [];


    for(let i = 0; i <= 10; i++) {
        let delayPushing = function () {
            result.push(i);
        }
        setInterval(delayPushing, 500)
    }

    return result; // числа от 0 до 10
}

/**
 * 7*. Реализовать такую же функцию, как letTimeout, только через var.
 * В комментарии объяснить, почему и как она работает
 */
//при полном копировании функции letTimeout, в итоговый массив попадает
//11 элементов "11", если я правильно поняла, то из-за всплытия перемен-
//ной i, объявленной с помощью ключевого слова var все элементы массива 
//переопределяются каждый проход цикла и на последнем проходе к финальному 
//значению добавляется еденица и записывается элемент "11" и, соответсвенно 
//переопределяются все значения массива
function varTimeout () {
    const result = [];

    for(var i = 0; i <= 10; i++) {
        let j = i;
        let delayPushing = function () {
            result.push(j);
        }
        setInterval(delayPushing, 500)
    }

    return result; // числа от 0 до 10
}

module.exports = {
    calculateHours: {
        onEarth: calculateHoursOnEarth,
        onMoon: calculateHoursOnMoon,
    },
    crashMeteorite,
    createMultiplier,
    createStorage,
    createCounter,
    letTimeout,
    varTimeout
};
